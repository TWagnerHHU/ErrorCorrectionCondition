﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingHypergraphStates{  

    class REWState
    {

        int qbits;
        bool[] Coefficients;   // eintrag i: Koeffizient des Computational Basis states nr. i, d.h von |j> wobei j die Darstellung von i in binär ist. false = +, true = -

        public REWState(int qbits)
        {
            this.qbits = qbits;
            Coefficients = new bool[(int)Math.Pow(2, qbits)];
        }

        public REWState(REWState R) //initalisiert eine copie von R
        {
            qbits = R.numberOfQubits();
            Coefficients = new bool[(int)Math.Pow(2, qbits)];
            bool[] r = R.getCoefficients();
            r.CopyTo(Coefficients, 0);            
        }
       

        public void SaveToFile(string path) // 1 = minus
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(path, true))
            {                

                for (int i = 0; i < Coefficients.Length; i++)
                {
                    if (Coefficients[i])
                    {
                       file.Write("1");
                    }
                    else
                    {
                        file.Write("0");
                    }
                }                
            }
        }

  /*      public void applyCe(int[] indizes)
        {            
            bool[] rep;
            bool flip;
            for(int i = 0;i<Coefficients.Length;i++)
            {
                flip = true;
                rep = binaryRepresentation(i);
                for (int j = 0; j < indizes.Length; j++)
                {
                    if (rep[indizes[j]] == false)
                    {
                        flip = false;
                    }
                }
                if (flip == true)
                {
                    Coefficients[i] = !Coefficients[i];
                }
            }                     
        }
  */

        //Wendet das generalisierte kontrollierte Z gate auf die durch indizes gegebenen qubits an, indizes beginnen bei 1
        public void applyCe(int[] indizes)//indizes beginnen bei 1
        {
            applyCe(indizes, qbits, 0);
        }

        private void applyCe(int[] indizes, int qbits, int n) 
        {
            if (qbits == 0)
            {
                Coefficients[n] = !Coefficients[n];
            }
            else
            {
                int m = n + (int)Math.Pow(2, qbits - 1);                
                applyCe(indizes, qbits - 1, m); //qbit ist 1
                if (!indizes.Contains(qbits)) //der Fall das der qbit 0 ist
                {
                    applyCe(indizes, qbits - 1, n);
                }
                
            }
        }

        public bool[] getCoefficients()
        {
            return Coefficients;
        }

        public int numberOfQubits()
        {
            return qbits;
        }

        public bool isOrthogonal(REWState s) 
        {
            int count = 0;
            bool[] b = s.getCoefficients();
            for (int i = 0; i < Coefficients.Length; i++)
            {
                if (b[i] == Coefficients[i])
                {
                    count++;
                }
                else
                {
                    count--;
                }
            }

            if (count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int scalarProdukt(REWState s)
        {
            int count = 0;
            bool[] b = s.getCoefficients();
            for (int i = 0; i < Coefficients.Length; i++)
            {
                if (b[i] == Coefficients[i]) //gleiches Vorzeichen gibt Beitrag +1
                {
                    count++;
                }
                else //verschiedenes Vorzeichen gibt Beitrag -1
                {
                    count--;
                }
            }
            return count;
        }


        //alte Methode
    /*    public void applyX(int index) // auch faul programmiert
        {
            bool[] b = new bool[Coefficients.Length];
            bool[] rep;
            
            for (int i = 0; i < Coefficients.Length; i++)
            {
                rep = binaryRepresentation(i);
                rep[index] = !rep[index];
                int j = binaryToIndex(rep);
                b[j] = Coefficients[i];
            }
            Coefficients = b;
        }
      */

        //wendet ein X gate auf qubit index an
        public void applyX(int index) // indizes beginnen bei 1
        {
            bool[] b = new bool[Coefficients.Length];           
            int n = (int)Math.Pow(2, index);
            bool temp;
            int t;
            for (int i = 0; i < Coefficients.Length; i++)
            {
                bool digitAtIndex = (((i % n)) >= (n/2)); //diese formel bestimmt ob der digit in binärrepresentation von i bei index eine 1 (true ) oder eine 0 (false) ist
                if (digitAtIndex == false)
                {
                    t = i + (int)(n / 2); //Index mit dem getauscht werden muss (flip des index von 0 auf eins ist addition von 2^(index-1))
                    temp = Coefficients[i];
                    Coefficients[i] = Coefficients[t];
                    Coefficients[t] = temp;
                }
            }
                
        }

        //alte Variante
      /*  public void setCoefficientForSize(bool coeff, int size) // true = -, false = +
        {
            Console.WriteLine("setting size " + size);
            bool[] rep;
            int count;
            for (int i = 0; i < Coefficients.Length; i++)
            {
                count = 0;
                rep = binaryRepresentation(i);
                for (int j = 0; j < rep.Length; j++)
                {
                    if (rep[j] == true)
                    {
                        count++;
                    }
                }
                if (count == size)
                {
                    Coefficients[i] = coeff;
                }
                if(i % 100 == 0)
                {
                    Console.WriteLine("Coeff " + i + " set");
                }
            }
        }
       */
        
        //alte variatne
       /* public void setCoefficientForSize(bool coeff, int size) // true = -, false = +
        {
            Console.WriteLine("setting size " + size);
            bool[][] binaries = getAllBinary(qbits, size);
            int index;
            for (int i = 0; i < binaries.Length; i++)
            {
                index=binaryToIndex(binaries[i]);
                Coefficients[index] = coeff;
            }         
         }
        */

        //Setz alle Coeffizienten von States mit weight "size" auf coeff
        public void setCoefficientForSize(bool coeff, int size) // true = -, false = +
        {
            setCoefficientForSize(coeff, size, 0, qbits);
        }

        private void setCoefficientForSize(bool coeff, int size, int n, int qbits)
        {            
            if (size == 0)
            {
                Coefficients[n] = coeff;
                return;
            }
            if (size > qbits)
            {
                return;
            }
            else
            {
                if (size >= 1) 
                {
                    int m = n + (int)Math.Pow(2, qbits - 1);                //Erster Qubit ist 1
                    setCoefficientForSize(coeff, size - 1, m, qbits - 1);
                }
                setCoefficientForSize(coeff, size, n, qbits - 1);           //Erster qubit ist 0
            }
        }

        public void setCoefficientForListOfSizes(bool coeff, int[] sizes)
        {
            for (int i = 0; i < sizes.Length; i++)
            {
                setCoefficientForSize(coeff,sizes[i]);
                Console.WriteLine("Size :" + sizes[i] + " set");
            }
        }


        public void printState()
        {
            bool[] rep;
            for (int i = 0; i < Coefficients.Length; i++)
            {

                rep = binaryRepresentation(i);
                for (int j = 0; j < rep.Length; j++)
                {
                    if (rep[j])
                    {
                        Console.Write("1");
                    }
                    else
                    {
                        Console.Write("0");
                    }
                }
                Console.Write(":");
                if (Coefficients[i])
                {
                    Console.WriteLine("-");
                }
                else
                {
                    Console.WriteLine("+");
                }
            }
        }


        private bool[] binaryRepresentation(int z) // z < 2^qbits, true = 1, false = 0
        {
            bool[] r = new bool[qbits];
            int temp;
            for (int i = 0; i < r.Length; i++)
            {
                temp = (int)Math.Pow(2, qbits - i-1);
                if (z >= temp)
                {
                    r[i] = true;
                }
                else
                {
                    r[i] = false;
                }
                z = (int)(z % temp);
            }
            return r;
        }

        private int binaryToIndex(bool[] binary)    //true = 1, false = 0
        {
            int count = 0;
            int result = 0;
            for (int i = binary.Length - 1; i >= 0; i--)
            {
                if (binary[i] == true)
                {
                    result = result + (int)Math.Pow(2, count);
                }
                count++;
            }

            return result;
        }

        //Beachte: nur sinvoll wenn der Zstd. symmetrisch ist
        public REWState constructH() //fasst diesen Zstd. als D auf und kosntruiert das entsprechende H gemäß der konstruktion in der Zusammenschrift (einen qubit mehr, edges ersetzen durch edges + qubit, symmetrisieren)
        {


            int[] edgeSizes = constructEdgeSizesSymmetrical(); //Berechnet die größen der Edges dieses Zustands
            for (int i = 0; i < edgeSizes.Length; i++)
            {
                edgeSizes[i] += 1;
            }


       //     Console.Write("edgeSizes: ");
       //     foreach(int i in edgeSizes)
       //     {
       //        Console.Write(i +" ");
       //     }
       //     Console.WriteLine();

            
            REWState H = new REWState(qbits + 1);            
            ulong sum;
            for (int i = 0; i <= H.numberOfQubits(); i++) //Iteriert über alle weights
            {
                sum = 0;                
                for (int j = 0; j < edgeSizes.Length; j++)
                {
                    sum += binom(i,edgeSizes[j]); //Summe über binomialcoeffs = Anzahl edges die auf zstd mit gewicht i wirken                    
                }
                if ((sum % 2) == 1) //ungerade Anzahl gates => negativer coeff
                {
                    H.setCoefficientForSize(true, i);
                   // Console.WriteLine("Coefficient for size " + i);
                }
            }

            return H;
        }

      


        public int[] constructEdgeSizesSymmetrical() //findet die größen der edges eine symmetrischen zustands
        {
            int n= 0;
            ulong sum;
            List<int> sizes = new List<int>();
            for (int i = 0; i <= qbits; i++) //iteriert über weights
            {
                sum = 0;
                foreach (int s in sizes)
                {
                    sum += binom(i, s);
                }
                bool sign = ((sum % 2) != 0); //Vorzeichen nach bisherigen edges
                if (Coefficients[n] != sign) //edge muss hinzugefügt werden um vorzeichen zu korrigieren, es ist egal welcher Coefficient mit weight i betrachtet wird wegen Symmetrie
                {
                    sizes.Add(i);
                }

                n += (int)Math.Pow(2, i);
            }
            return sizes.ToArray();
        }

        public void printEdgeSizesSymmetrical()
        {
            int[] t = constructEdgeSizesSymmetrical();
            Console.WriteLine();
            foreach (int i in t)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }
        
       // alte Methode       
   /*    public int[][] constructEdges() //liefert die Edges des zu diesem Zustand korrespondierendem Hypergraphen, gemäß dem algorithmus aus der Spezialisierung
       {  

            List<int[]> edges = new List<int[]>();  //edges die dieser Zustand hat
            int index;
            for (int i = 0; i <= qbits; i++) //gewichte der verschiedenen Zstd.
            {
                bool[][] binaries = getAllBinary(qbits, i);

                for (int j = 0; j < binaries.Length; j++)
                {
                    index = binaryToIndex(binaries[j]);
                    if (Coefficients[index])
                    {
                        int[] e = binaryToEdge(binaries[j]);
                        edges.Add(e); //fügt die entsprechende edge hinzu
                        Console.WriteLine("edge found");
                        this.applyCe(e); //wendet Ce an, so dass die Coeffizienten entsprechend angepasst werden für die weitere suche
                    }
                }
            }

            foreach (int[] e in edges)
            {
                applyCe(e);
            }
           return (edges.ToArray());
       }
 */

        //diese methode funktioniert wenn der Zustand symmetrisch ist
 /*       public int[][] constructEdgesSymmetrical()
        {
            
            int n = 0;
            bool[][] binaries;
            List<int[]> result = new List<int[]>();
            for (int i = 0; i <= qbits;i++)
            {
                if (Coefficients[n] == true)
                {
                    binaries = getAllBinary(qbits, i);
                    foreach (bool[] b in binaries)
                    {                       
                       
                        int[] e = binaryToEdge(b);  
                        applyCe(e);
                        result.Add(e);
                    }
                }
               n += (int)Math.Pow(2, i); //coeffient hängt nur vom gewicht ab
            }

            foreach (int[] e in result)
            {
                applyCe(e);
            }

            return result.ToArray();
        }
   */

     /*   public int[][] constructEdges() //veraltet
        {
            List<int[]> l = new List<int[]>();
            for (int i = 0; i <= qbits; i++)
            {
                constructEdgesOfSize(i, qbits, 0, l);
                Console.WriteLine("found edges of size " + i);
            }
            foreach (int[] e in l)
            {
                applyCe(e);
            }
            return l.ToArray();
        }
       */

  /*      private void constructEdgesOfSize(int size, int qbits, int n, List<int[]> l)
        {
            
            if (size > qbits)
            {
                return;
            }
            if (size == 0)
            {
                if (Coefficients[n] == true)
                {
                    Console.WriteLine("edge found " + n);
                    Console.WriteLine();
                    bool[] b = binaryRepresentation(n);                   
                    int[] e = binaryToEdge(b);
                    applyCe(e);
                    l.Add(e);
                }
            }
            else
            {
                int m = n + (int) Math.Pow(2,qbits-1);
                constructEdgesOfSize(size - 1, qbits - 1, m, l);
                constructEdgesOfSize(size, qbits - 1, n, l);                
            }
        }
     */  

  /*     public void printEdges() //Testmethode
       {
           int[][] e = constructEdges();

           for (int i = 0; i < e.Length; i++)
           {
               for (int j = 0; j < e[i].Length; j++)
               {
                   Console.Write(e[i][j] + " ");
               }
               Console.WriteLine();
           }
       }
   */

   /*    public void printEdgesSymmetrical() //Testmethode
       {
           int[][] e = constructEdgesSymmetrical();

           for (int i = 0; i < e.Length; i++)
           {
               for (int j = 0; j < e[i].Length; j++)
               {
                   Console.Write(e[i][j] + " ");
               }
               Console.WriteLine();
           }
       }
    */

       private int[] binaryToEdge(bool[] b) //gibt die edge zurück, die alle indizes enthält, bei denen im array der eintrag true ist (z.B. 101 => {0,2})
       {
           List<int> l = new List<int>();
           for(int i = 0;i<b.Length;i++)
           {
               if(b[i])
               {
                   l.Add(qbits-i);
               }
           }
           return(l.ToArray());
       }
    

        private bool[][] getAllBinary(int n, int w) //gibt alle binärzahlen mit n bits und gewicht w zurück , true = 1, false = 0
        {

            if ((n < w) || (n==0) )
            {
                return new bool[0][];
            }
          
            List<bool[]> l = new List<bool[]>();

            if (n == 1)
            {
                bool[] t = new bool[1];
                t[0] = (w == 1);
                l.Add(t);
            }
            else
            {

                if (w >= 1)
                {
                    bool[][] takeOne = getAllBinary(n - 1, w - 1);
                    for (int i = 0; i < takeOne.Length; i++)
                    {
                        bool[] t = new bool[n];
                        t[0] = true;
                        takeOne[i].CopyTo(t, 1);
                        l.Add(t);

                    }
                }

                bool[][] takeZero = getAllBinary(n - 1, w);
                for (int i = 0; i < takeZero.Length; i++)
                {
                    bool[] t = new bool[n];
                    t[0] = false;
                    takeZero[i].CopyTo(t, 1);
                    l.Add(t);

                }
            }

            return (l.ToArray());
        }

        private int[] getAllBinaryAsIndizes(int n, int w) // wie getAllBinary aber gibt nicht die binärrep zurück sondern die Zahlen
        {
            if ((n < w) || (n == 0))
            {
                return new int[0];
            }

            if (n == 1)
            {
                return new int[] { (int)Math.Pow(2, w) - 1 };
            }

            int[] results = new int[binom(n, w)];
            int count = 0;
            if (w >= 1)
            {
                int[] t = getAllBinaryAsIndizes(n - 1, w - 1);
                for (int i = 0; i < t.Length; i++)
                {
                    results[count]=(t[i] + (int)Math.Pow(2, n-1));
                    count++;
                }
            }

           int[] r = getAllBinaryAsIndizes(n - 1, w);
            for (int i = 0; i < r.Length; i++)
            {
                results[count] =r[i];
                count++;
            }

            return results;
        }

        public void printAllBinaryAsIndizes(int w) //Testmethode
        {
            int[] t = getAllBinaryAsIndizes(qbits, w);

            for (int i = 0; i < t.Length; i++)
            {
                Console.WriteLine(t[i]);
            }

        }

        public void printAllBinary(int w) //Testmethode
        {
            bool[][] t = getAllBinary(qbits, w);

            for (int i = 0; i < t.Length; i++)
            {
                for (int j = 0; j < t[i].Length; j++)
                {
                    if (t[i][j])
                    {
                        Console.Write("1");
                    }
                    else
                    {
                        Console.Write("0");
                    }
                }
                Console.WriteLine();
            }

        }

        public static ulong binom(int n, int k) // von C# funktioniert für große Zahlen, mindestens bis n = 50
        {

            if (n < k || k < 0)
            {
                return 0;
            }
            ulong result = 1;
            for (ulong i = 1; i <= (ulong)k; i++)
            {
                result *= (ulong)n - ((ulong)k - i);
                result /= i;
            }

            return result;
        }


    }
}
