﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestingHypergraphStates
{
    class Program

    {
        //Prüft für einen Dist 2 Code ob dieser Dist 3 hat
        static void Main(string[] args)
        {
    

            
            // Teste das zweite meiner Resulate für mögliche Dist 3 
            int l = 14; //Anzahl Z Gates
            REWState D = new REWState(29); //X-Differenz Zustand
            Console.WriteLine("D erzeugt");
            D.setCoefficientForListOfSizes(true, new int[] { 1, 3, 5, 7, 10, 12, 13, 15, 18, 20, 21, 23, 25, 27, 29 }); //Aus der Liste von Dist 2 Codes übernommen           
            Console.WriteLine("D initalisiert");       
         //   D.SaveToFile(@"C:\Users\User\Documents\Uni\QInfo\Ein Hypergraphencode\TestingHypergraphStates\TestingHypergraphStates\output\stateD.txt");
         //   Console.WriteLine("State saved");
     
            //initialisierung der Codewörter            
            REWState H = D.constructH(); //Konstruiert das erste Codeword aus dem X-Differenz Hypergraph
            Console.WriteLine("H initalisiert");
          //  H.SaveToFile(@"C:\Users\User\Documents\Uni\QInfo\Ein Hypergraphencode\TestingHypergraphStates\TestingHypergraphStates\output\stateH.txt");
          //  Console.WriteLine("State Saved");
            REWState G = new REWState(H);
            Console.WriteLine("G erzeugt");
            //Z Gates auf 1 bis l

            ///////////////////////////////
            //Indizes Beginnen Bei 1 !!!!!//
            ////////////////////////////////
            for (int i = 1; i <= l; i++)
            {
                G.applyCe(new int[] {i});
                Console.WriteLine("Z" + i);
            }
            Console.WriteLine("G initalisiert");

      
            
        REWState HT = new REWState(H);
            Console.WriteLine("HT erzeugt");
            bool o;
            bool dist2 = true;
            bool dist3 = true;



            //Teste Dist 3
            HT = new REWState(H);
            REWState GT = new REWState(G);
            //Z_iZ_j wird nicht getestet da das eh klar ist

            //Testing X_iZ_j (4 relevante kombinationen, jeweils i,j < l oder >l)            
            for (int i = 0; i <= 1; i++)
            {
                int indexX = i*(l) +1; //1 oder l+1
                HT.applyX(indexX);
                GT.applyX(indexX);
                for (int j = 0; j <= 1; j++)
                {
                    int indexZ = j*(l) + 2; //2 oder l+2
                    HT.applyCe(new int[] { indexZ });
                    GT.applyCe(new int[] { indexZ });
                    o = (HT.scalarProdukt(H) == GT.scalarProdukt(G));
                    Console.WriteLine("<H|X_" + indexX + "Z_" + indexZ + "H> == <G|X_" + indexX + "Z_" + indexZ + "G>: " + o);
                    if (o == false)
                        dist3 = false;
                    o = HT.isOrthogonal(G);
                    Console.WriteLine("G is orthogonal X_" + indexX + "Z_" + indexZ + "H: " + o);
                    if (o == false)
                        dist3 = false;

                    //Testing X_iY_j
                    HT.applyX(indexZ);
                    GT.applyX(indexZ);

                    o = (HT.scalarProdukt(H) == GT.scalarProdukt(G));
                    Console.WriteLine("<H|X_" + indexX + "Y_" + indexZ + "H> == <G|X_" + indexX + "Y_" + indexZ + "G>: " + o);
                    if (o == false)
                        dist3 = false;
                    o = HT.isOrthogonal(G);
                    Console.WriteLine("G is orthogonal X_" + indexX + "Y_" + indexZ + "H: " + o);
                    if (o == false)
                        dist3 = false;


                }
                HT = new REWState(H);
                GT = new REWState(G);
            }

            //Testing X_iX_j
            for (int i = 0; i <= 1; i++)
            {
                int indexX = i * (l) + 1; //1 oder l+1
                HT.applyX(indexX);
                GT.applyX(indexX);

                for (int j = 0; j <= 1; j++)
                {
                    int indexZ = j * (l) + 2; //2 oder l+2
                    HT.applyX(indexZ);
                    GT.applyX(indexZ);
                    o = (HT.scalarProdukt(H) == GT.scalarProdukt(G));
                    Console.WriteLine("<H|X_" + indexX + "X_" + indexZ + "H> == <G|X_" + indexX + "X_" + indexZ + "G>: " + o);
                    if (o == false)
                        dist3 = false;
                    o = HT.isOrthogonal(G);
                    Console.WriteLine("G is orthogonal X_" + indexX + "X_" + indexZ + "H: " + o);
                    if (o == false)
                        dist3 = false;
                }


                HT = new REWState(H);
                GT = new REWState(G);
            }

            Console.WriteLine("Distance 3 " + dist3);

            Console.ReadLine();
        }

        }
    }

